<?php
// $Id $

/**
 * @file one_comment_only.admin.inc
 * Functions that are only called on the admin pages.
 */

/**
 * Module settings form.
 */
function one_comment_only_settings() {

  $form['one_comment_only_info'] = array(
    '#value' => t('<p>Limits the number of comments that a user can post on a node.</p>')
    );
	
    
  // General settings.
  $form['general'] = array(
    '#type' => 'fieldset',
    '#title' => t('General settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    ); 
	
  $form['general']['one_comment_only_max'] = array(
    '#type' => 'textfield',
    '#title' => t('Maximum number of comments'),
    '#default_value' => variable_get('one_comment_only_max', 1),
    '#description' => t('What is the maximum number of comments?'),
    );
    
  $form['general']['one_comment_only_by_user'] = array(
    '#type' => 'checkbox',
    '#title' => t('Limit by user'),
    '#default_value' => variable_get('one_comment_only_by_user', false),
    '#description' => t('Do we limit the number of comments for each user (checked) or is this an overall limit (unchecked)?'),
    );
    
  $form['general']['one_comment_only_max_age'] = array(
    '#type' => 'textfield',
    '#title' => t('Time limit allowed for comments'),
    '#default_value' => variable_get('one_comment_only_max_age', ""),
    '#description' => t('Leave blank for indefinite time, enter date and time in format: <em>dd-mm-yyyy hh:mm:ss</em> or use relative time i.e. +1 day, +2 months, +1 year, +3 years'),
    );
  $form['general']['one_comment_only_relative'] = array(
    '#type' => 'checkbox',
    '#title' => t('Is the time limit relative?'),
    '#default_value' => variable_get('one_comment_only_relative', false),
    '#description' => t('Check if the time limit is relative to the publish date, i.e. +1 day, etc'),
    );

                 
  // Types settings.
  $form['types'] = array(
    '#type' => 'fieldset',
    '#title' => t('Types'),
  	'#collapsible' => TRUE,
    '#collapsed' => FALSE,
    );    
    
  $select = array();
  $types = node_get_types();
  $names = node_get_types('names');
  foreach ($types as $key => $type) {
    $select[$key] = $names[$key];
  } 
  $form['types']['one_comment_only_types'] = array(
    '#title' => t('Limited Types'),
    '#type' => 'checkboxes',
    '#default_value' => variable_get('one_comment_only_types', array()),
    '#options' => $select,
    '#description' => t('Select here which node types which you want to limit number of comments for.'),
    '#prefix' => '<div class="one_comment_only_checkboxes">',
    '#suffix' => '</div>',
    );  
    
  return system_settings_form($form);
}

/**
* Form validation function, we check if the limit format is ok
*/
function one_comment_only_settings_validate($form, &$form_state) {
  if (!is_numeric($form_state['values']['one_comment_only_max'])) {
    form_set_error('', t('You must enter an integer value for the limit.'));
  }else if ($form_state['values']['one_comment_only_max']<0) {
    form_set_error('', t('You must enter an positive value for the limit.'));
  }
}