<?php

/**
 * @file
 * Limits the number of comments that a user can post
 */

/*
 * Define the administrator UID
 */
define('ONE_COMMENT_ONLY_ADMIN_UID', 1);


/**
 * Display help and module information
 * @param path which path of the site we're displaying help
 * @param arg array that holds the current path as would be returned from arg() function
 * @return help text for the path
 */
function one_comment_only_help($path, $arg) {
	$output = '';  //declare your output variable
	
	switch ($path) {
		case "admin/help#one_comment_only":
			$output = '<p>'.  t("Limits the number of comments that a user can post on a node.") .'</p>';
		break;
	}
	
	return $output;
} // function one_comment_only_help

/**
 * Valid permissions for this module
 * @return array An array of valid permissions for the onthisdate module
 */
function one_comment_only_perm() {
	return array('administer one_comment_only');
} // function one_comment_only_perm()

/**
 * Implementation of hook_menu().
 */
function one_comment_only_menu() {
  $menu['admin/content/one_comment_only'] = array(
    'title' => 'One Comment Only',
    'description' => t('Configure One Comment Only'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('one_comment_only_settings'),
    'access arguments' => array('administer one_comment_only'),
    'file' => 'one_comment_only.admin.inc',
  );
  
  return $menu;
}

/**
 * Valid permissions for this module
 * @param $parent node we want to control comments limit
 * @param $user user we want to control comments limit
 * @param $error output error message
 *  * @return true if the user gets the right to publish
 */
function _one_comment_only_violates_limit($parent,$user,&$error){
	$ret=false;

	// we get settings
	$limitedTypes = variable_get('one_comment_only_types', array());
	$limit=variable_get("one_comment_only_max",1);
	$by_user=variable_get("one_comment_only_by_user",true);
	$max_age=variable_get("one_comment_only_max_age","");
	$relative=variable_get("one_comment_only_relative",false);
	
	
	
	// we check that the node type is under control and that the user is not the administrator
	// TODO : add a bypass for selected roles
	if ( $user->uid!=ONE_COMMENT_ONLY_ADMIN_UID && $limitedTypes[$parent->type]!="0" ){
	
		// we get number of user's nodes from the same type
		if ($by_user) $where="(nc.nid= %d AND nc.uid = %d)";
		else $where="nc.nid= %d";
	    $sql_count = 'SELECT COUNT(DISTINCT(nc.cid)) as c FROM {node_comments} nc WHERE '.$where;
	    $sql_count = db_rewrite_sql($sql_count);
	    $result =  db_query($sql_count, $parent->nid, $user->uid);
	    $count=db_fetch_object($result);
	    
	    // we get the max age of comments
	    if (strtotime($max_age)){
	    	if ($relative){
			    $publish_date=$parent->created;
			    $max_time=strtotime($max_age)-time();
			    $max_time=time()-($publish_date+$max_time);
	    	}else{
	    		$max_time=time()-strtotime($max_age);
	    	}
		    if ($max_time>0){
				$error=t("You can't create comment for this !type anymore, time limit is exhausted", array('!type' => $parent->type));
			    $ret=true;
		    }
	    }
	   
	    // we check the limit
	    if ($count->c>$limit){
			if ($by_user) $error=t("You can't create more than !limit comment(s) for this !type",array('!limit' => $limit,'!type' => $parent->type));
			else $error=t("The overall limit of !limit comment(s) has been reached, you can't comment anymore this !type.", array('!limit' => $limit,'!type' => $parent->type));
	    	$ret=true;
	    }
	    
	}
	return $ret;
	
}

/**
 * Implementation of hook_nodeapi().
 *
 * This is where we'll determine if the user may create new nodes or not.
 * We'll key off $op == 'prepare', which is sent before the edit/add form
 * is constructed.
 */
function one_comment_only_nodeapi(&$node, $op, $a3 = NULL, $a4 = NULL) {
	global $user;
		
	// we want to control only comment, nodes limits are provided by node_limit otherwise
	if ($node->comment_target_cid==0 && $op == 'presave') {
		// we get the parent node
		$pid=$node->comment_target_nid;
		
		$parent=node_load($pid);
		// we prepare error message
		$error="";
		
		// we check the limit and the node pre-existence
	  	if ( !isset($node->nid) && _one_comment_only_violates_limit($parent,$user,$error) ) {
		    // we have a violation and this is a new node
			drupal_set_message($error, 'error');
			// we return to the node page
		    drupal_goto('node/'.arg(3));
	  	}
	}
}
